import os

train = 'data/en-ud-train.conllu'
dev = 'data/owoputi.integrated'

for tweets in [True]:#, False]:
    for seed in range(1,11):
        seed = str(seed)
        for char in [True, False]:
            for ext in [True, False]:
                modelDir = 'models/' + seed
                if tweets:
                    modelDir += '.tweets'
                if char:
                    modelDir += '.char'
                if ext:
                    modelDir += '.ext'
                cmdTrain = 'python3 src/parser.py --seed ' + seed + ' --train ' + train + ' --dev ' + dev + ' --usehead --userlmost --embedConv --out ' + modelDir
                cmdRun = 'python3 src/parser.py --seed ' + seed + ' --runAll ' + dev + ' --model ' + modelDir + '/model --out ' + 'preds/' + modelDir[modelDir.rfind('/')+1:]
                if tweets:
                    cmdTrain += ' --tweets'
                    cmdRun += ' --tweets'
                if char == False:
                    cmdTrain += ' --cembedding=0 --chlstmdims=0'
                if ext:
                    cmdTrain += ' --extrn /data/p270396/wang.100.5.txt'
                cmdClean = "rm -r " + modelDir
                print(cmdTrain, end = ' && ')
                print(cmdRun, end = '  ')
                #print(cmdClean)

                print()

