# If more than 5 arguments are given, it will run using slurm
function run {
    if [ "$6" ];
    then
        python3 scripts/pg.prep.py $1 $2 $3 $4 $5
        python3 scripts/pg.run.py $2.[0-9]*
    else
        chmod +x $1
        bash $1
    fi
}

./scripts/0.norm.prep.sh
./scripts/0.norm.train.sh > 0.train.sh
run 0.train.sh 0.train 2 40 8 $1

run scripts/0.norm.run.sh 0.run 2 20 2 $1
./scripts/0.norm.integrate.sh

./scripts/1.dev.getEmbeds.sh
python3 ./scripts/1.dev.run.py > 1.dev.sh
run 1.dev.sh 1.dev 15 140 1

python3 scripts/2.test.run.py > 2.test.sh
run 2.test.sh 2.test 15 140 1

