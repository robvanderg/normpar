import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import myutils

if len(sys.argv) < 3:
    print('Please give folder, and name of corpus')
    exit(1)

def getScore(path):
    score = 0.0
    if not os.path.isfile(path):
        return None
    for line in open(path):
        if (line.split()[0] =="LAS"):
            line.replace('|','')
            score = float(line.split()[-1])
            if score < 1:
                score *=100
    return score

adaptModesTweets = ['tweets', 'tweets.char', 'tweets.ext', 'tweets.char.ext']
adaptNames = [r'$\vec{w}$', r'$\vec{w}+\vec{c}$', r'$\vec{w}+\vec{e}$', r'$\vec{w}+\vec{c}+\vec{e}$']
normNames = [r'Orig', 'Norm', 'Integrated', 'Gold']
normModes = ['0.0', '1.0', '2.10', '3.0']

def getScores(path, normMode):
    data = []
    for i in adaptModesTweets:
        total = 0
        divider = 0
        for seed in range(0,10):
            if seed == 5:
                continue
            newScore = getScore(path + str(seed) + '.' + i + '/' + sys.argv[2] + '.integrated.' + normMode + '.txt')
            if newScore != None:
                total += newScore
                divider += 1
        data.append(total/divider)
    return data

fig, ax = plt.subplots(figsize=(8,5), dpi=300)
bar_width=.2

for idx, normMode in enumerate(normModes):
    data = getScores(sys.argv[1] + '/', normMode)
    index = []
    for i in range(len(data)):
        #index.append(.1 + i + bar_width * idx)
        index.append(i + bar_width * (idx + 1))
    print(normMode)
    print (data)
    print(index)
    ax.bar(index, data, bar_width, color=myutils.colors[idx], linewidth=3, label=normNames[idx])

myutils.setTicks(ax, adaptNames)

ax.set_ylabel("LAS")
ax.set_xlabel("Vector Contents")
ax.set_ylim((53,63)) 
ax.set_xlim((0,4))
ax.legend(loc='upper left', title='Normalization')
ax.grid(True)

fig.savefig('bars.pdf',bbox_inches='tight')

