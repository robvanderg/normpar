import matplotlib.pyplot as plt

def getScore(path):
    score = 0.0
    for line in open(path):
        if (line.split()[0] =="LAS"):
            line.replace('|','')
            score = float(line.split()[-1])
            if score < 1:
                score *=100
    return score

def getScores(path):
    scores = []
    for i in range(1,11):
        scores.append(getScore(path + '/owoputi.integrated.2.' + str(i) + '.txt'))
    return scores

realNames = ['base', '+char', '+ext', '+char+ext']
for idx, modelName in enumerate(['3.tweets', '3.tweets.char', '3.tweets.ext', '3.tweets.char.ext']):
    plt.plot(range(1,11), getScores('preds/' + modelName), label = realNames[idx])

plt.savefig('topn.pdf',,bbox_inches='tight')
plt.show()

