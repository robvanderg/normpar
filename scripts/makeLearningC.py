
import matplotlib.pyplot as plt
import sys
import os

def getline(path):
    data = [0] * 31
    for resFile in os.listdir(path):
        if not resFile.endswith('.txt'):
            continue
        
        iteration = resFile[resFile.rfind('_')+1:]
        iteration = int(iteration[:iteration.find('.')])
        for line in open(path + '/' + resFile):
            if (line.split()[0] =="LAS"):
                line.replace('|','')
                score = float(line.split()[-1])
                if score < 1:
                    score *=100
                data[iteration] = score
    name = path[path.find('//')+1:-1]
    return data, name


for i in range(1, len(sys.argv)):
    data, name = getline(sys.argv[i])
    print (name, data)
    plt.plot(range(len(data)), data, linewidth=3, label=name)

plt.ylim(45,65)
plt.legend(loc='lower right')
plt.savefig('graph.pdf',bbox_inches='tight')
plt.show()

