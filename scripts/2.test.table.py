import conll17_ud_eval as conll17
from scipy import stats
import os


def getTrees(conlPath):
    tree = ''
    trees = []
    for line in open(conlPath):
        if line[0] == '#':
            continue
        elif len(line) < 2:
            trees.append(tree)
            tree = ''
        else:
            tree += line
    return trees

def scoreTrees(predTrees, goldTrees):
    if not os.path.isfile(predTrees):
        return [], []
    weights=["aux 0.1", "case 0.1", "cc 0.1", "clf 0.1", "cop 0.1", "det 0.1", "mark 0.1", "punct 0"]
    conll17.load_deprel_weights(weights)#TODO check if this works?
    LAS = []
    UAS = []
    for goldTree, predTree in zip(getTrees(goldTrees), getTrees(predTrees)):
        open('gold', 'w').write(goldTree + '\n')
        open('pred', 'w').write(predTree + '\n')
        goldConl = conll17.load_conllu(open('gold', 'r'))
        predConl = conll17.load_conllu(open('pred', 'r'))
        result = conll17.evaluate(goldConl, predConl)
        LAS.append(result['LAS'].f1)
        UAS.append(result['UAS'].f1)
    return UAS + UAS, LAS + LAS

def ttest(list1, list2):
    return stats.ttest_rel(list1,list2)


def getScores(path):
    UAS = 0.0
    LAS = 0.0
    for line in open(path):
        if line.startswith('UAS'):
            UAS = float(line.split()[-1])
        if line.startswith('LAS'):
            LAS = float(line.split()[-1])
    return UAS * 100, LAS * 100

print('\\begin{table}')
print('    \\center')
print('    \\begin{tabular}{l | l l | l l}')
print('        \\toprule')
print('                             & \multicolumn{2}{c}{dev} & \multicolumn{2}{c}{test} \\\\')
print('        Model                & UAS        & LAS        & UAS        & LAS \\\\')
print('        \\midrule')
setups = ['0.0', '1.0', '2.10', '3.0']
setupNames = ['Orig', 'Norm', 'Integrated', 'Gold']
prevTreeScores = []
for setup, name in zip(setups, setupNames):
    predDev = 'preds/6.tweets.char.ext/owoputi.integrated.' + setup
    predTest = 'preds/test.6.tweets.char.ext/lexnorm.integrated.' + setup
    scores = list(getScores(predDev + '.txt') + getScores(predTest + '.txt'))
    treeScores = list(scoreTrees(predDev, 'data/owoputi.conllu') + scoreTrees(predTest, 'data/lexnorm.conllu'))
    strRow = [] 
    for scoreIdx, score in enumerate(scores):
        sign = ''
        if len(prevTreeScores) > 0:
            if ttest(treeScores[scoreIdx], prevTreeScores[scoreIdx])[1]/2 < 0.05:
                sign = '\\textsuperscript{*}'
        strRow.append(('{0:.2f}'.format(score) + sign).ljust(10))
    if name == 'Gold':
        print('        \\midrule')
    print('        \\textsc{' + (name + '}').ljust(12) + ' & ' + ' & '.join(strRow) + ' \\\\')
    prevTreeScores = treeScores

print('        \\bottomrule')
print('    \\end{tabular}')
print('    \\caption{UAS and LAS for the Twitter development and test treebanks.')
print('\\textsuperscript{*}Statistically significant compared to the previous row at')
print('$p < 0.05$ using a paired t-test.}')
print('    \\label{tab:test}')
print('\\end{table}')

