wget http://www.robvandergoot.com/data/tweets.en.w2v.bin
git clone https://github.com/marekrei/convertvec.git
cd convertvec
make
./convertvec bin2txt ../tweets.en.w2v.bin ../tweets.en.w2v.txt
cd ..
rm tweets.en.w2v.bin
sed -i -e "1d" tweets.en.w2v.txt
rm -rf convertvec
