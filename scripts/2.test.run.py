import os

train = 'data/en-ud-train.conllu'
test = 'data/lexnorm.integrated'

for tweets in [True]:#, False]:
    seed = '6'
    char = True
    ext = True
    tweets = True
    modelDir = 'models/test.' + seed
    if tweets:
        modelDir += '.tweets'
    if char:
        modelDir += '.char'
    if ext:
        modelDir += '.ext'
    cmdTrain = 'python3 src/parser.py --seed ' + seed + ' --train ' + train + ' --dev ' + test + ' --usehead --userlmost --embedConv --out ' + modelDir
    cmdRun = 'python3 src/parser.py --seed ' + seed + ' --runAll ' + test + ' --model ' + modelDir + '/model --out ' + 'preds/' + modelDir[modelDir.rfind('/')+1:]
    if tweets:
        cmdTrain += ' --tweets'
        cmdRun += ' --tweets'
    if char == False:
        cmdTrain += ' --cembedding=0 --chlstmdims=0'
    if ext:
        cmdTrain += ' --extrn tweets.en.w2v.txt'
    cmdClean = "rm -r " + modelDir
    print(cmdTrain, end = ' && ')
    print(cmdRun, end = '  ')
    #print(cmdClean)

    print()

