import sys
from decimal import *

if len(sys.argv) < 3:
    print('please give output of MoNoise with -c <num> and coresponding connlu file')
    exit(1)


conlFile = open(sys.argv[2], encoding='utf-8', errors='ignore')

norms = []
probs = []
conllu = []
for line in open(sys.argv[1], encoding='utf-8', errors='ignore'):
    tok = line.split()
    if len(tok) > 1:
        pos = int(tok[0])  
        if pos < 2:
            continue
        pos = pos -2
        norm = tok[1].replace('|','')
        prob = tok[-1]
        if pos >= len(norms):
            norms.append([])
            probs.append([])
        if prob >= '0.00001' and prob != '-0.0':
            norms[pos].append(norm)
            probs[pos].append(prob)
    else:
        # read conllu
        for conlLine in conlFile:
            conlTok = conlLine.strip().split('\t')
            if len(conlTok) < 2:
                if conlLine[0] == '#':
                    print(conlLine.strip())
                else:
                    break
            else:
                conllu.append(conlTok)
        for wordIdx in range(len(conllu)):
            norm = ''
            if wordIdx < len(norms):
                for candIdx in range(len(norms[wordIdx])):
                    norm += '|cand' + str(candIdx) + '=' + norms[wordIdx][candIdx]
                    norm += '|candP' + str(candIdx) + '=' + probs[wordIdx][candIdx]
            if (conllu[wordIdx][9] == '_'):
                print ("\t".join(conllu[wordIdx][:-1] + [norm[1:-1]]))
            else:
                print ("\t".join(conllu[wordIdx]) + norm[:-1])
        print()
        norms = []
        probs = []
        conllu = []

