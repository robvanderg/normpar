from collections import Counter
import re, time, random
import conll17_ud_eval as conll17

class ConllEntry:
    def __init__(self, tok):
        self.id = int(tok[0])
        self.form = tok[1]
        self.lemma = tok[2]
        self.pos = tok[3]
        self.cpos = tok[4]
        self.feats = tok[5]
        self.parent_id = 0 if tok[6] == '_' else int(tok[6])
        self.relation = tok[7]
        self.advanced = tok[8]
        self.misc = tok[9]

        self.normCands = []
        self.normProbs = []
        self.goldNorm = ''
        for miscItem in self.misc.split('|'):
            if miscItem.startswith('candP'):
                candIdx = int(miscItem[5:miscItem.find('=')])
                prob = float(miscItem[miscItem.find('=') + 1:])
                if candIdx >= len(self.normProbs):
                    self.normProbs.append(0.0)
                self.normProbs[candIdx] = prob
            elif miscItem.startswith('cand'):
                candIdx = int(miscItem[4:miscItem.find('=')])
                candStr = miscItem[miscItem.find('=') + 1:]
                if candIdx >= len(self.normCands):
                    self.normCands.append('')
                self.normCands[candIdx] = candStr
            elif miscItem.startswith('Norm='):
                self.goldNorm = miscItem[5:]

        self.pred_parent_id = None
        self.pred_relation = None


    def __str__(self):
        values = [str(self.id), self.form, self.lemma, \
                  self.pos, self.cpos, \
                  self.feats, str(self.pred_parent_id), \
                  self.pred_relation, \
                  self.advanced, self.misc]
        return '\t'.join(['_' if v is None else v for v in values])

class ParseForest:
    def __init__(self, sentence):
        self.roots = list(sentence)

        for root in self.roots:
            root.children = []
            root.scores = None
            root.parent = None
            root.pred_parent_id = None
            root.pred_relation = None
            root.vecs = None
            root.lstms = None

    def __len__(self):
        return len(self.roots)


    def Attach(self, parent_index, child_index):
        parent = self.roots[parent_index]
        child = self.roots[child_index]

        child.pred_parent_id = parent.id
        del self.roots[child_index]


def vocab(conll_path, embedConv):
    wordsCount = Counter()
    charsCount = set()
    rels = set()

    for sentence in read_conll(conll_path):
        if embedConv:
            wordsCount.update([normalize(node.form, embedConv) for node in sentence if isinstance(node, ConllEntry)])
        else:
            wordsCount.update([node.form for node in sentence if isinstance(node, ConllEntry)])
        for node in sentence:
            if isinstance(node, ConllEntry):
                for char in node.form:
                    charsCount.add(char)
                rels.add(node.relation)

    vocab = {}
    vocab['*PAD*'] = 1
    vocab['*INITIAL*'] = 2
    for i, word in enumerate(wordsCount.keys()):
        vocab[word] = i + 3

    chars = {ind: word + 3 for word, ind in enumerate(charsCount)} 
    
    return (wordsCount, vocab, chars, sorted(list(rels)))


def read_conll(conlPath):
    ts = time.time()
    read = 0
    root = ConllEntry([0, '*root*', '*root*', 'ROOT-POS', 'ROOT-CPOS', '_', -1, 'rroot', '_', '_'])
    tokens = [root]
    for line in open(conlPath):
        tok = line.strip().split('\t')
        if not tok or line.strip() == '':
            if len(tokens)>1:
                conll_tokens = [t for t in tokens if isinstance(t,ConllEntry)]
                inorder_tokens = inorder(conll_tokens)
                for i,t in enumerate(inorder_tokens):
                    t.projective_order = i
                for tok in conll_tokens:
                    tok.rdeps = [i.id for i in conll_tokens if i.parent_id == tok.id]
                    if tok.id != 0:
                        tok.parent_entry = [i for i in conll_tokens if i.id == tok.parent_id][0]
                yield tokens
                read += 1
            tokens = [root]
        else:
            if line[0] == '#' or '-' in tok[0] or '.' in tok[0]:
                tokens.append(line.strip())
            else:
                token = ConllEntry(tok)
                tokens.append(token)
    if len(tokens) > 1:
        yield tokens
    te = time.time()
    print (read, 'sentences read.')
    print ('Time: %.2gs'%(te-ts))


def write_conll(fn, conll_gen):
    with open(fn, 'w') as fh:
        for sentence in conll_gen:
            for entry in sentence[1:]:
                fh.write(str(entry) + '\n')
            fh.write('\n')

numberRegex = re.compile("[0-9]+|[0-9]+\\.[0-9]+|[0-9]+[0-9,]+");
def normalize(word, embedConv):
    if embedConv and word[0] == '@':
        return '<USERNAME>'
    if embedConv and word.find('http') > 0 or word.find('www') > 0:
        return '<URL>'    
    return 'NUM' if numberRegex.match(word) else word.lower()



def evaluate(gold,test,writeFile):
    weights=["aux 0.1", "case 0.1", "cc 0.1", "clf 0.1", "cop 0.1", "det 0.1", "mark 0.1", "punct 0"]

    conll17.load_deprel_weights(weights)#TODO check if this works?
    goldData = conll17.load_conllu(open(gold, 'r'))
    testData = conll17.load_conllu(open(test, 'r'))
    result = conll17.evaluate(goldData, testData)

    outFile = open(writeFile,'w')
    for item in result:
        outFile.write(item + '\t' + str(result[item].precision) + '\t' + str(result[item].recall) + '\t' + str(result[item].f1) + '\n')
    outFile.close()
    return result['UAS'].f1, result['LAS'].f1

def inorder(sentence):
    queue = [sentence[0]]
    def inorder_helper(sentence,i):
        results = []
        left_children = [entry for entry in sentence[:i] if entry.parent_id == i]
        for child in left_children:
            results += inorder_helper(sentence,child.id)
        results.append(sentence[i])

        right_children = [entry for entry in sentence[i:] if entry.parent_id == i ]
        for child in right_children:
            results += inorder_helper(sentence,child.id)
        return results
    return inorder_helper(sentence,queue[0].id)

