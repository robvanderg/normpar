from optparse import OptionParser
from arc_hybrid import ArcHybridLSTM
import pickle, utils, os, time
import conll17_ud_eval as conll17

def train(options):
    if not (options.rlFlag or options.rlMostFlag or options.headFlag):
        raise Exception("You must use either --userlmost or --userl or\
                            --usehead (you can use multiple)")
    if options.out == None:
        raise Exception ('Please use --out, to specify to which folder the model should be written')
    if not os.path.isdir(options.out):
        os.makedirs(options.out)
    if options.params == None:
        options.params = os.path.join(options.out, 'params.pickle')


    print ('Preparing vocab')
    wordCounts, vocab, chars, rels = utils.vocab(options.conll_train, options.embedConv)
    pickle.dump((wordCounts, vocab, chars, rels, options), open(options.params, 'wb'))
    print ('Finished collecting vocab')

    print ('Initializing blstm arc hybrid:')
    parser = ArcHybridLSTM(wordCounts, vocab, chars, rels, options)

    highestScore = 0.0
    for epoch in range(options.epochs):
        print ('Starting epoch', epoch)
        traindata = list(utils.read_conll(options.conll_train))
        parser.Train(traindata)
        if os.path.exists(options.conll_dev):
            devName = os.path.basename(options.conll_dev)
            outPath = os.path.join(options.out, devName + "_" + str(epoch + 1))
            devData = enumerate(utils.read_conll(options.conll_dev))
            utils.write_conll(outPath, list(parser.Predict(devData)))
            uas, las = utils.evaluate(outPath, options.conll_dev, outPath + ".txt")
            print(uas, las)

            """
            parser.setNorm(1,1,1)
            outPath = os.path.join(options.out, devName + ".norm_" + str(epoch + 1))
            devData = enumerate(utils.read_conll(options.conll_dev))
            utils.write_conll(outPath, list(parser.Predict(devData)))
            uas, las = utils.evaluate(outPath, options.conll_dev, outPath + ".txt")
            print(uas, las)
            parser.setNorm(0,0,1)
            """

            if las > highestScore:
                parser.Save(os.path.join(options.out, "model"))
                highestScore = las
            print ('Finished predicting dev')
        else:
            parser.Save(os.path.join(options.out, "model" ))

def loadParser(options):
    if options.model == None:
        raise Exception ('Please use --model')

    if options.params == None:
        #TODO fix when not using '/' at end
        if os.path.isfile(options.model[:options.model.rfind('/') + 1] + 'params.pickle'):
            options.params = options.model[:options.model.rfind('/') + 1] + 'params.pickle'
        else:
            raise Exception ('Please use --params')

    wordCounts, vocab, chars, rels, stored_opt = pickle.load(open(options.params, 'rb'))

    stored_opt.normMode = options.normMode
    stored_opt.normWeight = options.normWeight
    stored_opt.tweets = options.tweets
    parser = ArcHybridLSTM(wordCounts, vocab, chars, rels, stored_opt)
    parser.Load(options.model)
    return parser

def predict(options):
    if options.out == None:
        raise Exception ('Please use --out, to specify where to write to')
    parser = loadParser(options)
    ts = time.time()
    testData = enumerate(utils.read_conll(options.conll_pred))
    prediction = parser.Predict(testData)
    utils.write_conll(options.out, prediction)
    print ('Finished predicting test to ' + options.out, str(time.time() - ts))
    uas, las = utils.evaluate(options.out, options.conll_pred, options.out + ".txt")
    print (uas, las)


# helper for runAll
def runOne(testPath, parser, options, normMode, normWeight):
    parser.setNorm(normMode, normWeight, options.tweets)
    testData = enumerate(utils.read_conll(testPath))
    prediction = parser.Predict(testData)
    fileName = os.path.join(options.out, os.path.basename(options.conll_runAll) + '.' + str(normMode) + '.' + str(normWeight))
    utils.write_conll(fileName, prediction)
    score = utils.evaluate(fileName, options.conll_runAll, fileName + ".txt")
    print(fileName)
    print(score)

def runAll(options):
    if options.out == None:
        raise Exception ('Please use --out, to specify to which folder the results should be written')
    if not os.path.isdir(options.out):
        os.makedirs(options.out)
    parser = loadParser(options)

    runOne(options.conll_runAll, parser, options, 0,0)
    runOne(options.conll_runAll, parser, options, 1, 0)
    for i in range(1,11):
        runOne(options.conll_runAll, parser, options, 2, i)
    runOne(options.conll_runAll, parser, options, 3, 0)



if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("--train", dest="conll_train", help="Train new model, give training data as argument, and specify --dev for tuning and --out with destination path")
    parser.add_option("--pred", dest='conll_pred', help='Write output, give input data as argument, use with --out and --model')
    parser.add_option("--runAll", dest="conll_runAll", help="Run all different types of normalization, give output of integrateNorm.py as argument. use with --out and --model")

    parser.add_option("--model", dest="model", help="Load model file")
    parser.add_option("--params", dest="params", help="Load parameters file")
    parser.add_option("--dev", dest="conll_dev", help="Annotated CONLL dev file")
    parser.add_option("--extrn", dest="external_embedding", help="External embeddings")
    parser.add_option("--out", dest="out", help='Path to write predictions')

    parser.add_option("--normMode", type="int", dest="normMode", default=0, help='Use normalization: 0=none 1=direct 2=interpolation 3=gold')
    parser.add_option("--normWeight", type="float", dest="normWeight", default=1.0, help='Only used with --normMode =2/3, respecively weight, or top-n of candidates')
    parser.add_option("--tweets", action="store_true", dest="tweets", default=False, help='Use some twitter specific hardcoded rules')
    parser.add_option("--embedConv", action="store_true", dest="embedConv", default=False, help='Convert Twitter usernames to <USERNAME> and urls to URL before searching in the embeddings')

    parser.add_option("--wembedding", type="int", dest="wembedding_dims", default=100, help='Dimension of word embeddings')
    parser.add_option("--rembedding", type="int", dest="rembedding_dims", default=15, help='Dimension of relations')
    parser.add_option("--lstmdims", type="int", dest="lstm_dims", default=100, help='Word BI-LSTM Dimensions')
    parser.add_option("--cembedding", type="int", dest="cembedding_dims", default=12, help='Dimensions of character embeddings')
    parser.add_option("--chlstmdims", type="int", dest="chlstm_dims", default=50, help='Character BI-LSTM Dimensions')

    parser.add_option("--epochs", type="int", dest="epochs", default=30, help='Number of training iterations')
    parser.add_option("--hidden", type="int", dest="hidden_units", default=100, help='Hidden units in MLP')
    parser.add_option("--hidden2", type="int", dest="hidden2_units", default=0, help='Add a second hidden layer')
    parser.add_option("--k", type="int", dest="window", default=3, help='Number of words on the stack to keep into account')
    parser.add_option("--lr", type="float", dest="learning_rate", default=0.001, help='Learning rate')
    parser.add_option("--activation", type="string", dest="activation", default="tanh", help='Activation, one of [tanh, sigmoid, relu, tanh3]')
    parser.add_option("--seed", type="int", dest="seed", default=123456789, help='Seed for random and dynet library')
    parser.add_option("--disableoracle", action="store_false", dest="oracle", default=True, help='Disable the use of dynamic oracle')
    parser.add_option("--usehead", action="store_true", dest="headFlag", default=False, help='Use the BiLSTM of the head of trees on the stack as feature vectors')
    parser.add_option("--userl", action="store_true", dest="rlFlag", default=False, help='Add the BiLSTM of the right/leftmost children to the feature vectors')
    parser.add_option("--userlmost", action="store_true", dest="rlMostFlag", default=False, help='Add the vector of the right/leftmost children to the feature vectors')

    (options, args) = parser.parse_args()

    if options.conll_train != None:
        train(options)
    elif options.conll_pred != None:
        predict(options)
    elif options.conll_runAll != None:
        runAll(options)
    else:
        print ('please specify at least one of --train --test --pred --run --runAll')

