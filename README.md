# NormParser
## Integrating Normalization in Transition based dependency parsing.
This parser is based on the [UUParser](https://github.com/UppsalaNLP/uuparser) version 2.0, which was in turn based on the [BIST](http://github.com/elikip/bist-parser). Our main contribution is the ability to integrate normalization in the parsing process. See for more detailed information the paper: [Modeling Input Uncertainty in Neural Network Dependency Parsing](http://www.let.rug.nl/rob/doc/emnlp2018.pdf). However, also some other things are changed, see the `Notes' section of this document.

#### Treebank
The annotated treebank can be found in the data folder: owoputi.conllu is the development data and lexnorm.conllu the test data. The *.integrated files contain the automatic normalization in the misc column.

#### Required software

 * Python 2.7 or 3 interpreter
 * [DyNet library](https://github.com/clab/dynet/tree/master/python)

#### Options

All options can be seen by: python3 src/parser --help:
```
> python3 ~/projects/normPar/src/parser.py  --help
Usage: parser.py [options]

Options:
  -h, --help            show this help message and exit
  --train=CONLL_TRAIN   Train new model, give training data as argument, and
                        specify --dev for tuning and --out with destination
                        path
  --pred=CONLL_PRED     Write output, give input data as argument, use with
                        --out and --model
  --runAll=CONLL_RUNALL
                        Run all different types of normalization, give output
                        of integrateNorm.py as argument. use with --out and
                        --model
  --model=MODEL         Load model file
  --params=PARAMS       Load parameters file
  --dev=CONLL_DEV       Annotated CONLL dev file
  --extrn=EXTERNAL_EMBEDDING
                        External embeddings
  --out=OUT             Path to write predictions
  --normMode=NORMMODE   Use normalization: 0=none 1=direct 2=interpolation
                        3=gold
  --normWeight=NORMWEIGHT
                        Only used with --normMode =2/3, respecively weight, or
                        top-n of candidates
  --tweets              Use some twitter specific hardcoded rules
  --embedConv           Convert Twitter usernames to <USERNAME> and urls to
                        URL before searching in the embeddings
  --wembedding=WEMBEDDING_DIMS
                        Dimension of word embeddings
  --rembedding=REMBEDDING_DIMS
                        Dimension of relations
  --lstmdims=LSTM_DIMS  Word BI-LSTM Dimensions
  --cembedding=CEMBEDDING_DIMS
                        Dimensions of character embeddings
  --chlstmdims=CHLSTM_DIMS
                        Character BI-LSTM Dimensions
  --epochs=EPOCHS       Number of training iterations
  --hidden=HIDDEN_UNITS
                        Hidden units in MLP
  --hidden2=HIDDEN2_UNITS
                        Add a second hidden layer
  --k=WINDOW            Number of words on the stack to keep into account
  --lr=LEARNING_RATE    Learning rate
  --activation=ACTIVATION
                        Activation, one of [tanh, sigmoid, relu, tanh3]
  --seed=SEED           Seed for random and dynet library
  --disableoracle       Disable the use of dynamic oracle
  --usehead             Use the BiLSTM of the head of trees on the stack as
                        feature vectors
  --userl               Add the BiLSTM of the right/leftmost children to the
                        feature vectors
  --userlmost           Add the vector of the right/leftmost children to the
                        feature vectors
```

#### Train

python3 src/parser.py --train data/en-ud-train.conllu --dev data/lexnorm.integrated --usehead --userlmost --out modelDir --tweets 

Use --extrn to exploit external word embeddings. 

#### Parse data with your parsing model

To run on 1 conllu file:
```
python3 src/parser.py --pred data/lexnorm.integrated --out outFile --model modelDir --tweets
```

To run all the normalization experiments. Note that normalization is necessary (see next section).
```
python3 src/parser.py --runAll data/lexnorm.integrated --out outDir --model modelDir --tweets
```

To run a specific setting use --normMode and --normWeight

#### Reproduction of paper results:
TODO

#### Integrate normalization into conllu file

If you want to use the existing data, just use data/owoputi.integrated or data/lexnorm.integrated.
If you want to run the normalization yourself, see scripts/0.norm.*


#### Notes
There are some differences compared to the uuparser:

 * the parameters usage is quite different (parser.py is almost completely redone)
 * not all models are saved, this resulted in huge files when using external embeddings
 * all multilingual things are removed to make it easier to edit the code
 * POS tags are now completely removed from the code
 * seeding is done more consistent
 * works now on python3, performance is approximately the same
 * Made it possible to disable character level information by using " --cembedding=0 --chlstmdims=0 " 

#### Citation
```
@InProceedings{goot-noord:2018:EMNLP2018,
  author    = {van der Goot, Rob  and  van Noord, Gertjan},
  title   = {Modeling Input Uncertainty in Neural Network Dependency Parsing},
  booktitle = {Proceedings of the 2018 Conference on Empirical Methods in Natural Language Processing},
  month     = {October},
  year      = {2018},
  address   = {Brussels, Belgium},
  publisher = {Association for Computational Linguistics},
  pages     = {},
  url2       = {https://www.aclweb.org/anthology/D17-1001}
}
```

#### License

This software is released under the terms of the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

